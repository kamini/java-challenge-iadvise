const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
	hallType:{
		type:String,
		unique: true
	},
	description:{
		type:String
	},
	calendar : [{
		type:Schema.Types.ObjectId, 
		ref:"Reservation"
	}],
	containsHalls : [{
		type:Schema.Types.ObjectId,
		ref:"Hall"
	}],
	userPrice:{
		type:Number
	},
	businessPrice:{
		type:Number
	},
	isDeleted : {
		type:Boolean,
        default: false
	}

});
module.exports = Hall = mongoose.model("Hall", schema);