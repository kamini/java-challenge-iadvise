const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Hall = require("./hall")
const OpeningHours = require("./opening-hour");
const DateFormat = require('dateformat');

const schema = new Schema({
    user: {
        type: Schema.Types.Mixed
    },
    email:{
        type: String
    },
    halls: [{
        type: Schema.Types.ObjectId,
        ref:'Hall', required: true
    }],
    reason: {
        type:String,
        required:true
    },
    approved: {
        type:Boolean,
        required:true,
        default:false
    },
    reserveDate:{
        type:Date,
    },
	startDate:{
		type:Date,
		required:true
	},
	endDate:{
		type:Date,
		required:true
	},
    isPartOfReservation:{
        type:Schema.Types.ObjectId,
        ref: 'Reservation',
        default:null
    }
});

module.exports = Reservation = mongoose.model('Reservation', schema)

module.exports.checkHallDatesOverlap = async function(halls, beginDate, endDate){
    let beginDayTime = new Date(new Date(beginDate).setHours(0,0,0,0));
    let endDayTime = new Date(new Date(beginDate).setHours(23,59,59,999));
    for (let i in halls){
        let hall = halls[i];
        let reservations = await Reservation.find({halls:hall._id, startDate:{$gt:beginDayTime, $lt:endDayTime}});
        for (let i in reservations){
            let reservation = reservations[i];
            if (reservation.approved){
                if (beginDate < reservation.endDate && reservation.startDate < endDate){
                    throw "Dates overlap";
                }
            }
        }
    }
};

module.exports.checkOpeningHoursOverlap = async function (startDate, endDate) {
    let dayOfTheWeek = DateFormat(startDate, "N") -1
    let openingHours = await OpeningHours.findOne({day: dayOfTheWeek});

    if(DateFormat(startDate, "HH:MM") < openingHours.openTime || DateFormat(endDate,"HH:MM") > openingHours.closeTime)
    {
        throw "Hall not open on that time"
    }
};

module.exports.getWeekReservationsByHallId = async function (hallId, offset = new Date()) {
    let beginDayTime = new Date(new Date().setUTCDate(offset.getDate() - offset.getDay()));
    beginDayTime.setUTCHours(0,0,0,0);
    let endDayTime = new Date(new Date().setUTCDate(offset.getDate() + 7 - offset.getDay()));
    endDayTime.setUTCHours(23,59,59,999);

    let reservations = await Reservation.find({startDate:{$gt:beginDayTime, $lt:endDayTime}, halls: hallId});
    return reservations;
}
/*
module.exports.checkCoRentable = function(halls){
    if (halls.length > 1){    
        // Fill co rentable halls with objects
        for (let i in halls){
            if (halls[i].coRentableHalls == null || halls[i].coRentableHalls.length == 0){
                throw "Cant rent some halls simultaniously";
            }
        }
        
        let checkedHalls = [];
        let loopOverHalls;

        function pushToChecked(hall){
            checkedHalls.push(hall._id.toString());
            loopOverHalls();
        }

        function getHallById(hallId){
            for (let i in halls){
                if (halls[i]._id.toString() == hallId.toString()){
                    return halls[i];
                }
            }
        }

        loopOverHalls = function(){
            for (let i in halls){
                console.log(checkedHalls);
                if (checkedHalls.indexOf(halls[i]._id) < 0){
                    for (let c in checkedHalls){
                        let thisHall = getHallById(checkedHalls[c]);
                        console.log("found hall");
                        if (thisHall && thisHall.coRentableHalls.indexOf(halls[i].toString()) != -1){
                            pushToChecked(halls[i]);
                        }
                    }
                }4
            }
        }

        pushToChecked(halls[0]);

        console.log(halls);
        console.log(checkedHalls);

        for (let i in halls){
            if (checkedHalls.indexOf(halls[i]._id.toString()) == -1){
                throw "Cant rent some halls simultaniously";
            }
        }
    }
}*/