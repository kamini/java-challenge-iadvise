const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({    
    firstName: {
        type:String
    },
    lastName: {
        type:String
    },
    email: {
        type:String,
        unique:true
    },
    role:{
        type: String,
        default: 'user',
        enum: ['user','admin']
    }
})


module.exports = User = mongoose.model("User", schema);
