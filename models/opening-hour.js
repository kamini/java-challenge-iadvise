const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
    day:{
        type:Number,
        unique:true
    },
    openTime:{
        type:String
    },
    closeTime:{
        type:String
    }
});

module.exports = OpeningHours = mongoose.model('OpeningHour', schema);
