const Reservation = require("../models/reservation");

module.exports.findReservation = async function(req,res,next){
	try{
		let reservationId = req.params.reservationId || req.body.reservationId;
		let reservation = await Reservation.findById(reservationId);
		if (!reservation){
			throw "reservation does not exist";
		}
		req.reservation = reservation;
		next();
	}catch(err){
		res.status(400).json({err});
	}
}