const Hall = require("../models/hall");

module.exports.findHall = async function(req,res,next){
	try{
		let hallId = req.params.hallId || req.body.hallId;
		let hall = await Hall.findById(hallId);
		if (!hall){
			throw "Hall does not exist";
		}
		req.hall = hall;
		next();
	}catch(err){
		res.status(400).json({err});
	}
}

module.exports.findHalls = async function(req,res,next){
	try{
		let hallIds = req.body.hallIds;
		let halls = await Hall.find({_id: hallIds});
		if (halls.length == 0){
			throw "Hall does not exist";
		}
		req.halls = halls;
		next();
	}catch(err){
		res.status(400).json({err});
	}
}
