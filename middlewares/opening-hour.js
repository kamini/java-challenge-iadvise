const OpeningHour = require("../models/opening-hour");

module.exports.findOpeningHour = async function(req,res,next){
    try{
        let openinghourId = req.params.openingHourId || req.body.openingHourId;
        let openingHour = await OpeningHour.findById(openinghourId);
        if (!openingHour){
            throw "Openinghour does not exist";
        }
        req.openingHour = openingHour;
        next();
    }catch(err){
        res.status(400).json({err});
    }
}