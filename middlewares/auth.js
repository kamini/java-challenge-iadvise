const User = require("../models/user");

module.exports.auth = async function(req,res,next){
	try{
		let user = await User.findById(req.headers.authorization);
		if (!user){
			throw "Invalid user";
		}
		req.user = user;
		next();
	}catch(err){
		res.status(401).json({err});
	}
}

module.exports.adminAuth = async function(req,res,next){
	try{
		let user = await User.findById(req.headers.authorization);
		if (!user || user.role != "admin"){
			throw "Invalid user";
		}
		req.user = user;
		next();
	}catch(err){
		res.status(401).json({err});
	}
}