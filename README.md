----------------
Deploy
----------------

run once:
	make sure you got angular-cli installed "npm i -g @angular/cli"
run once every update:
	run "npm install" in both the root folder and the angular-src folder
	run "npm run build" inside the angular-src folder
run everytime:
	start local mongodb database with default settings
	run "npm run start" inside the root folder
	open browser and connect to http://localhost:3000

Gmail account
    To check the send messages go to google.mail.com and log in with 'LGUtestmail@gmail.com' and password 'LGUAcademy'
    and go to the sent mails

credentials
    LGUadmin@gmail.com
    LGUuser@gmail.com

Changing language:
	Click on the button in the navigation bar that says "nederlands" or "english"

Logging in:
	1. Click on the login button in navigation bar
	2. Write your email address and write a random string for the password
	3. Click on the sign in button

Creating a reservation:
	optional: login
	1. Go to the homepage and select a hall
	2. Click on a column on the date you want to create a reservation
	3. Fill in the form and click on the submit button (If you're not logged in you will have to fill in an additional field for your email address)
	4. Go to the mail account listed in the beginning to check if the mail has been send

View reservations:
    1. Log in with a user
    2. Click on "Reservations" in the navigation bar

Edit reservations:
    1. Log in with a user (that has a reservation that is not approved)
    2. Click on "Reservations" in the navigation bar
    3. Click "Edit" on the reservation you want to edit
    4. Change the things you want to change
    5. Click "Submit" to save the changes

Approve reservations:
    1. Log in as administrator
    2. Click on "Pending reservations" in the navigation bar
    3. Approve a reservation by clicking "Approve" or deny one by clicking "Delete"
    4. Go to the mail account listed in the beginning to check if the mail has been send

Create a hall:
    1. Log in as administrator
    2. Go to the homepage and click on "New hall"
    3. Fill in the form and click on the save button

Edit a hall:
    1. Log in as administrator
    2. Go to the homepage and select "Edit" on a hall
    3. Edit what you want to edit
    4. Click "Submit" to save the changes

Delete a hall:
    1. Log in as administrator
    2. Go to the homepage and click on "Delete" for which hall you want to delete

