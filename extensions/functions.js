module.exports.mergeDbObjects = function(obj1, obj2){
	let keys = Object.keys(obj2);
	let obj1Keys = Object.keys(obj1._doc);
	for(let i=0;i<keys.length;i++){
		let key = keys[i];
		if (obj1Keys.indexOf(key) > -1 && key != "_id" && key != "__v" ){
			obj1[key] = obj2[key];
		}
	}
}

module.exports.putDbObjects = function(obj1, obj2){
	let keys = Object.keys(obj1);
	for(let i=0;i<keys.length;i++){
		let key = keys[i];
		if (key != "_id" && key != "__v" ){
			obj1[key] = obj2[key];
		}
	}
}