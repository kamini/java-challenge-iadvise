const nodemailer = require('nodemailer');

module.exports.sendMail = function(to, subject, content) {

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'lgutestmail@gmail.com',
            pass: 'LGUAcademy'
        },
        tls: { rejectUnauthorized: false }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"LGU Academy" <menuonweb@gmail.com>', // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: content, // plain text body
        html: '<table border="0" style="background-color: lightgray" width="100%" cellpadding="20px">' +
                '<tbody>' +
                    '<tr>' +
                        '<td align="center">' +
                            '<table border="0">' +
                                '<tbody>' +
                                    '<tr>' +
                                        '<td><img src="cid:mailImage"/></td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td align="center">' + content + '</td>' +
                                    '</tr>' +
                                '</tbody>' +
                            '</table>' +
                        '</td>' +
                    '</tr>' +
                '</tbody>' +
             '</table>', // html body
        attachments: [{
            filename: 'lgu-banner-logo.png',
            path:'./../java-challenge-iadvise/angular-src/src/assets/images/lgu-banner-logo.png',
            cid: 'mailImage'
        }]
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    });
}