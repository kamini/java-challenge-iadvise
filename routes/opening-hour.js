const express = require('express');
const OpeningHour = require('../models/opening-hour');
const router = express.Router();
const mAuth = require("../middlewares/auth");
const mOpeningHour = require("../middlewares/opening-hour");
const functions = require("../extensions/functions");

async function init() {
    if ((await OpeningHour.find()).length == 0) {
        const openingHour = new OpeningHour({
            day:0,
            openTime:"09:00",
            closeTime: "22:00"
        });

        await openingHour.save();
        for(let i = 1; i < 7; i++) {
            openingHour._id=null;
            openingHour.day = i;
            openingHour.isNew = true;
            await openingHour.save();
        }
    }
}

init();



router.get('/', async function(req,res){
   try
   {
       let openingHour = await OpeningHour.find();
       if(!openingHour)
       {
           throw 'There are no openinghour!';
       }
       res.json(openingHour);
   }
   catch (err)
   {
       res.status(500).json({
           title: 'Error in getting openinghour!',
           error: err
       })
   }
});

router.get('/:day', async function(req,res){
    try
    {
        let openingHour = await OpeningHour.find().where({day:req.params.day});
        if(!openingHour)
        {
            throw 'There are no openinghour!';
        }
        res.json(openingHour);
    }
    catch (err)
    {
        res.status(500).json({
            title: 'Error in getting openinghours!',
            error: err
        })
    }
});

router.post('/',mAuth.adminAuth, async function(req,res){
    try
    {
        if(req.body.day >= 0 || req.body.day <7) {
            req.body._id = null;
            let openingHour = new OpeningHour(req.body);
            openingHour =  await openingHour.save();
            res.json(openingHour);
        }
        else
            throw "This is not a right day of the week";
    }
    catch (err)
    {
        res.status(500).json({
            title: 'Error in posting openinghour!',
            error: err
        })
    }
});


router.put('/:openingHourId',mAuth.adminAuth, mOpeningHour.findOpeningHour, async function(req,res){
    try
    {
        req.openingHour.day = req.body.day;
        req.openingHour.openTime = req.body.openTime;
        req.openingHour.closeTime = req.body.closeTime;

        req.OpeningHour = await req.openingHour.save();
        res.json(req.openingHour);
    }
    catch (err)
    {
        res.status(500).json({
            title: 'Error in putting openinghour!',
            error: err
        })
    }
});

router.patch('/:openingHourId',mAuth.adminAuth, mOpeningHour.findOpeningHour, async function(req,res){
    try
    {
        functions.mergeDbObjects(req.openingHour, req.body);
        req.openingHour.save();
        res.json(req.openingHour);
    }
    catch (err)
    {
        res.status(500).json({
            title: 'Error in patch openinghour!',
            error: err
        })
    }
});


router.delete('/:openingHourId',mAuth.adminAuth, mOpeningHour.findOpeningHour, async function(req,res){
    try
    {
        req.openingHour.remove();
        res.json();
    }
    catch (err)
    {
        res.status(500).json({
            title: 'Error in patch openinghour!',
            error: err
        })
    }
});
module.exports = router;


