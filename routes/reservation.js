const express = require("express");
const Reservation = require("../models/reservation");
const Hall = require("../models/hall");
const User = require("../models/user");
const mHall = require("../middlewares/hall");
const mReservation = require("../middlewares/reservation");
const functions = require("../extensions/functions");
const mAuth = require("../middlewares/auth");
const nodemailer = require('./../extensions/nodemailer');
const emailValidator = require('email-validator');

const router = express.Router();

router.get("/", async function(req,res){
	try{
		let reservations = await Reservation.find();
		res.json(reservations)
	}catch(err){
		res.status(500).json({err});
	}
});


router.get("/approved/", mAuth.adminAuth, async function(req, res){
    try
    {
        let reservations = await Reservation.find({isPartOfReservation: null, startDate: {$gt:new Date()}, approved:false}).sort({reserveDate:1})
        res.json(reservations);
    }
    catch (err)
    {
        console.log(err);
        res.status(400).json({err});
    }
});

router.get("/user/", mAuth.auth, async function(req,res){
    try
    {
        let reservations = await Reservation.find({user: req.user._id, startDate: {$gt: new Date()}}).sort({startDate:-1});
        res.json(reservations);
    }
    catch (err)
    {
        res.status(400).json({err});
    }
});

router.get("/:id", async function(req,res){
	try{
		let reservation = await Reservation.findById(req.params.id);
		if (!reservation){
			throw "Reservation does not exist";
		}
		
		res.json(reservation)
	}catch(err){
		res.status(500).json({err});
	}
});

router.post("/", mHall.findHalls, async function(req,res){
	try {
		//om veel code veranderingen te vermijden
		req.hall = req.halls[0];

        let startDate = new Date(req.body.startDate);
        let endDate = new Date(req.body.endDate);
        if (startDate >= endDate) {
            throw "beginDate is later then endDate";
        }
        await Reservation.checkOpeningHoursOverlap(startDate, endDate);
        await Reservation.checkHallDatesOverlap(req.halls, startDate, endDate);
        if(req.hall.containsHalls!=0)
		{
            if(req.hall.containsHalls!=null)
            {
                await Reservation.checkHallDatesOverlap(req.hall.containsHalls, startDate, endDate);
            }
		}
        let reservation = new Reservation();
        let date = new Date().getTime();
        reservation.reserveDate = new Date(date + (60*60*1000)).toISOString();
        reservation.halls = req.halls;
        reservation.startDate = startDate;
        reservation.endDate = endDate;
        reservation.reason = req.body.reason;
		let user = await User.findById(req.headers.authorization);
		if(user.role == "admin")
		{
			reservation.approved = true;
		}

        if (user != null)
		{
            reservation.user = user._id;
		}
		else if(req.body.email !=null)
		{
			if(emailValidator.validate(req.body.email))
				reservation.user = req.body.email;
			else
				throw "Not an email";
		}
		else
		{
			throw "No user or email was given";
		}
		await reservation.save();
        reservation = await Reservation.findOne().sort({_id:-1});
		let maillist = "";
		let name;
		if(user != null)
		{
            name = user.lastName + " " + user.firstName;
		}
		else
		{
			name = req.body.email;
		}
		const admins = await User.find({role:"admin"});
		for(let i = 0; i < admins.length; i++)
		{
			if(maillist != "")
			{
				maillist += ", "
			}
			maillist += admins[i].email;
		}

        let rooms = "";
        for(let i = 0; i <  reservation.halls.length; i++)
        {
            if(rooms != "")
            {
                rooms += " and ";
            }

            rooms += (await Hall.findById(reservation.halls[i])).hallType;
        }

        nodemailer.sendMail(
            maillist,
            'Reservation made by ' + name,
            '<h4>Reservation made by ' + name + '</h4>' +
			'<p>Room: ' + rooms + '</p>' +
			'<p>Start date: ' + reservation.startDate.toLocaleDateString() + " " + reservation.startDate.toLocaleTimeString() + '</p>' +
            '<p>End date: ' + reservation.endDate.toLocaleDateString() + " " + reservation.endDate.toLocaleTimeString() + '</p>'
        );


		if(req.hall.containsHalls != null)
		{
            for(let i=0; i < req.hall.containsHalls.length; i++)
            {
				let partReservation = new Reservation();

                if (user != null)
                {
                    reservation.user = user._id;
                }
                else
                {
                    reservation.user = req.body.email;
                }

                partReservation.reserveDate = new Date(date + (60*60*1000)).toISOString();
                partReservation.halls = req.hall.containsHalls[i];
                partReservation.startDate = startDate;
                partReservation.endDate = endDate;
                partReservation.reason = req.body.reason;
                partReservation.isPartOfReservation = reservation;

				if(user.role == "admin")
				{
					reservation.approved = true;
				}

                console.log(partReservation);
                partReservation.save();
            }
		}


		res.json(reservation);
	}catch(err){
		console.log(err);
		res.status(400).json({err});
	}
});

router.put("/:reservationId", mAuth.auth, mReservation.findReservation, async function(req, res){
	console.log(req.body);
	try{
		let approvedBefore = req.reservation.approved;
		functions.mergeDbObjects(req.reservation, req.body);
		req.reservation.approved = approvedBefore;
		await Reservation.checkOpeningHoursOverlap(req.reservation.startDate, req.reservation.endDate);
        await Reservation.checkHallDatesOverlap(req.reservation.halls, req.reservation.startDate, req.reservation.endDate);
        req.reservation.save();
        res.json(req.reservation);
	}catch(err){
		console.log(err);
		res.status(500).json({err});
	}
});

router.patch("/:reservationId", mAuth.adminAuth, mReservation.findReservation, async function(req, res){
	try{
		let approvedBefore = req.reservation.approved;
		functions.mergeDbObjects(req.reservation, req.body);
		await req.reservation.save();
		if(approvedBefore != req.reservation.approved &&approvedBefore ==false)
		{
			console.log(req.reservation.user)
			let user = await User.findById(req.reservation.user);
			let rooms = "";
			for(let i = 0; i <  req.reservation.halls.length; i++)
			{
				if(rooms != "")
				{
					rooms += " and ";
				}

                rooms += (await Hall.findById(req.reservation.halls[i])).hallType;
			}
			let partReservations = await Reservation.find({isPartOfReservation: req.reservation._id});
            if(partReservations != null) {
                for (let i = 0; i < partReservations.length; i++) {
                    let reservation = partReservations[i];
                    console.log(reservation);
                    reservation.approved = req.body.approved;
                    reservation.save();
                }
            }

			nodemailer.sendMail(
                    user.email,
					'Reservation Approved',
                	'<h4>Your reservation for ' + rooms + ' has been approved!</h4>'
        		);

		}
		res.json(req.reservation);
	}catch(err){
		console.log(err);
		res.status(500).json({err});
	}
});

router.delete("/:reservationId", mAuth.adminAuth, mReservation.findReservation, async function(req,res){
	try{
		await req.reservation.remove();

        let rooms = "";
        for(let i = 0; i <  req.reservation.halls.length; i++)
        {
            if(rooms != "")
            {
                rooms += " and ";
            }

            rooms += (await Hall.findById(req.reservation.halls[i])).hallType;
        }

        let user = await User.findById(req.reservation.user);

        nodemailer.sendMail(
            user.email,
            'Reservation Denied',
            '<h4>Your reservation for ' + rooms + ' has been Denied!</h4>'
        );

        let partReservations = await Reservation.find({isPartOfReservation: req.reservation._id});
        if(partReservations != null) {
            for (let i = 0; i < partReservations.length; i++) {
                let reservation = partReservations[i];
                reservation.remove();
            }
        }


		res.json({success:true});
	}catch(err){
		console.log(err);
		res.status(500).json({err});
	}
});

module.exports = router;