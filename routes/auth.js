const express = require("express");
const User = require("../models/user");
const router = express.Router();
const mAuth = require("../middlewares/auth");

async function init(){
	let id = "000000000000000000000000";
	let user = await User.findById(id);
	if (!user){
		user = new User();
		user._id = id;
	}
	user.email = "LGUadmin@gmail.com";
	user.firstName = "admin";
	user.lastName = "admin";
	user.role = "admin";
	await user.save();

	id = "000000000000000000000001";
	user = await User.findById(id);
	if (!user){
		user = new User();
		user._id = id;
	}
	user.email = "LGUuser@gmail.com";
	user.firstName = "Bob";
	user.lastName = "De Bouwer";
	user.role = "user";
	await user.save();


}

router.get("/", mAuth.auth, function(req,res){
	console.log("getting user");
	res.json(req.user);
});

init();

router.post("/", async function(req,res){
	try{
		let user = await User.find({email: req.body.email});
		if (!user && user.length != 1){
			throw "User does not exist";
		}
		res.json(user[0]);
	}catch(err){
		res.status(500).json({err});
	}
});

module.exports = router;