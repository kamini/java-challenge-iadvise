const express = require('express');
const Hall = require('../models/hall');
const Reservation = require('../models/reservation');
const router = express.Router();
const mAuth = require("../middlewares/auth");
const mHall = require("../middlewares/hall");
const functions = require("../extensions/functions");

async function init()
{
 if((await Hall.find()).length==0)
 {
     let hall = new Hall();
     hall.hallType = "Mediaruimte";
     hall.description = "Has PC's";
     hall.userPrice = 10;
     hall.businessPrice= 15;
     hall.save();

     hall = new Hall();
     hall.hallType = "Bar/Lounge";
     hall.description = "";
     hall.userPrice = 10;
     hall.businessPrice= 15;
     hall.save();

     hall = new Hall();
     hall.hallType = "Studio";
     hall.description = "";
     hall.userPrice = 10;
     hall.businessPrice= 15;
     hall.save();

     hall = new Hall();
     hall.hallType = "Akoustische ruimte";
     hall.description = "";
     hall.userPrice = 10;
     hall.businessPrice= 15;
     hall.save();

     let hall1 = new Hall();
     hall1.hallType = "Danszaal 1";
     hall1.description = "";
     hall1.userPrice = 10;
     hall1.businessPrice= 15;

     let hall2 = new Hall();
     hall2.hallType = "Danszaal 2";
     hall2.description = "";
     hall2.userPrice = 10;
     hall2.businessPrice= 15;


     let hall3 = new Hall();
     hall3.hallType = "Danszaal 3";
     hall3.description = "";
     hall3.userPrice = 10;
     hall3.businessPrice= 15;

     await hall1.save();
     hall1 = await Hall.findOne().sort({_id:-1});
     await hall2.save();
     hall2 = await Hall.findOne().sort({_id:-1});
     await hall3.save();
     hall3 = await Hall.findOne().sort({_id:-1});

     hall = new Hall();
     hall.hallType = "Feestzaal";
     hall.description = "Contains Danszaal 1, Danszaal 2 and Danszaal 3";
     hall.userPrice = 10;
     hall.businessPrice= 15;

     hall.containsHalls.push(hall1._id);
     hall.containsHalls.push(hall2._id);
     hall.containsHalls.push(hall3._id);

     hall.save();


 }
}

init();

router.get("/", async function (req, res) {
    try {
        let halls = await Hall.find().where({isDeleted:false});
        if(!halls)
        {
            throw "No halls exist!";
        }
        res.json(halls);
    }
    catch(err)
    {
        res.status(500).json({
            title: 'Error in getting Halls',
            error: err
        })
    }

});

router.get("/:id", async function(req, res)
{
    try
    {
       let hall = await Hall.findById(req.params.id);
       if(!hall)
       {
           throw "Hall does not exist!"
       }
       let offsetDate = new Date();
	   if (req.query.offset)
	   		offsetDate = new Date(req.query.offset);	
	   hall.calendar = await Reservation.getWeekReservationsByHallId(hall._id, offsetDate);
       res.json(hall);
    }
    catch(err) {
        res.status(500).json({
            title: 'Error in getting Hall',
            error: err
        })
    }
})

router.post("/", mAuth.adminAuth, async function(req, res)
{
    try
    {
        req.body._id = null;
        let hall = new Hall(
            req.body
        );

        hall = await hall.save();
        res.json(hall);
    }
    catch(err)
    {
        res.status(500).json({
            title: 'Error in posting Hall',
            error: err
        })
    }
});

router.put("/:hallId", mAuth.adminAuth, mHall.findHall , async function (req, res) {
    try {

        req.hall.hasPcs= req.body.hasPcs;
        req.hall.calendar= req.body.calendar;
        req.hall.containsHalls= req.body.containsHalls;
        req.hall.userPrice= req.body.userPrice;
        req.hall.businessPrice= req.body.businessPrice;
        req.hall.hallType = req.body.hallType;
        req.hall.description = req.body.description;

        req.hall = await req.hall.save();
        res.json(req.hall);
    }
    catch (err)
    {

        res.status(500).json({
            title: 'Error in putting Hall',
            err
        })
    }


});

router.patch("/:hallId", mAuth.adminAuth, mHall.findHall, async function (req,res) {
    try
    {
        functions.mergeDbObjects(req.hall, req.body);
        req.hall = await req.hall.save();
        res.json(req.hall);
    }
    catch (err)
    {
        res.status(500).json({
            title: 'Error in patching Hall',
            error: err
        })
    }
})

router.delete("/:hallId", mAuth.adminAuth, mHall.findHall, async function(req,res){
    try
    {
        req.hall.isDeleted = true;
        await req.hall.save();

        res.json(req.hall);
    }
    catch (err)
    {
        res.status(500).json({
            title: 'Error in deleting hall',
            error: err
        })
    }
})

module.exports = router;


