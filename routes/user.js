const express = require("express");
const User = require("../models/user");
const router = express.Router();


router.get("/:userId", async function(req,res){
    try{
        let user = User.findById(req.params.userId);
        res.json(user);
    }catch(err){
        res.status(500).json({err});
    }
});

module.exports = router;