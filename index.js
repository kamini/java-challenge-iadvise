const express = require("express");
const http = require("http");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const auth = require("./routes/auth");
const hall = require("./routes/hall");
const reservation = require("./routes/reservation");
const openingHour = require('./routes/opening-hour');
const mail = require("./routes/user");

const app = express();

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/iadvise", { useMongoClient: true }, function(err) {
    if (!err)
        console.log("connected to database");
    else
        console.log("NOT connected to database");
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/auth", auth);
app.use("/hall", hall);
app.use("/reservation", reservation);
app.use("/openinghour", openingHour);
app.use("/mail", mail);
app.use("/", express.static("public"));

const httpServer = http.createServer(app);
httpServer.listen(3000, function() {
    console.log("Server started");
});